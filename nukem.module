<?php

/**
 * @file
 * Allows you to remove a themes persistent variables if the theme is disabled,
 * including color module variables. This could be refactored and be better, but
 * for now it does the job I need it too just fine, patches welcome.
 */

/**
 * Implements hook_help().
 */
function nukem_help($path, $arg) {
  switch ($path) {
    case 'admin/help#nukem':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Nukem module allows you to delete themes persistent variables, which are stored in the variables table in the database. These variables are not deleted by Drupal when you disable a theme so must be removed manually (such as using Nukem). Nukem will remove Color module variables and the files directory variable set by Adaptivetheme.');
      return $output;
    case 'admin/appearance/nukem':
      return '<p>' . t('The Nukem module allows you to delete themes persistent variables, which are stored in the variables table in the database. These variables are not deleted by Drupal when you disable a theme so must be removed manually. Includes the removal of Color module variables.') . '</p>';
  }
}

/**
 * Implements hook_menu().
 */
function nukem_menu() {
  $items['admin/appearance/nukem'] = array(
    'title' => 'Nukem',
    'description' => 'Allows you to remove theme variables not currently used.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('nukem_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Return a list of disabled themes that have a variable table entry
 */
function nukem_get_disabled_themes() {
  $themes = list_themes();
  $disabled_themes = array();
  foreach ($themes as $theme => $info) {
    if ($info->status == 0 && variable_get('theme_' . $theme . '_settings', NULL)) {
      $disabled_themes[$theme] = $theme;
    }
  }

  return $disabled_themes;
}

// Form
function nukem_form($form, &$form_state) {

  // Get disabled themes that have a variable table entry
  $options = nukem_get_disabled_themes();

  $form['nukem'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme variables to delete'),
    '#tree' => TRUE,
  );
  if (!empty($options)) {
    $form['nukem']['options'] = array(
      '#type' => 'checkboxes',
      '#title' => t('These themes are disabled and have a variable table entry, all persistent variables will be deleated. Warning - there is no confirmation screen - all data will be nuked!'),
      '#options' => $options,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }
  else {
    $form['nukem']['message'] = array(
      '#markup' => t('Nothing to delete. You need to disable themes before deleting persistent variables, even so no variables might exist if the theme settings were never saved.'),
    );
  }
  $form['#submit'][] = 'nukem_form_submit';

  return $form;
}

// Form submit
function nukem_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  foreach ($values['nukem']['options'] as $theme_name => $value) {
    if ($value !== 0) {
      nukem_variables_to_delete($value);
    }
  }
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
}

// Delete variables
function nukem_variables_to_delete($theme) {

  $deleted = array();
  // regular theme variable
  if (variable_get('theme_' . $theme . '_settings')) {
    variable_del('theme_' . $theme . '_settings');
    $deleted[] = 'theme_' . $theme . '_settings';
  }
  // Adaptivetheme theme variable for files directory
  if (variable_get('theme_' . $theme . '_files_directory')) {
    variable_del('theme_' . $theme . '_files_directory');
    $deleted[] = 'theme_' . $theme . '_files_directory';
  }
  // Color module persistent variables
  if (module_exists('color')) {
    if (variable_get('color_' . $theme . '_files')) {
      variable_del('color_' . $theme . '_files');
      $deleted[] = 'color_' . $theme . '_files';
    }
    if (variable_get('color_' . $theme . '_logo')) {
      variable_del('color_' . $theme . '_logo');
      $deleted[] = 'color_' . $theme . '_logo';
    }
    if (variable_get('color_' . $theme . '_palette')) {
      variable_del('color_' . $theme . '_palette');
      $deleted[] = 'color_' . $theme . '_palette';
    }
    if (variable_get('color_' . $theme . '_stylesheets')) {
      variable_del('color_' . $theme . '_stylesheets');
      $deleted[] = 'color_' . $theme . '_stylesheets';
    }
    if (variable_get('color_' . $theme . '_screenshot')) {
      variable_del('color_' . $theme . '_screenshot');
      $deleted[] = 'color_' . $theme . '_screenshot';
    }
  }
  if (!empty($deleted)) {
    $deleted = theme('item_list', array('items' => $deleted));
  }
  drupal_set_message(t('<p>Deleted theme persistent variables for <b>@theme</b>:</p>!deleted_vars', array('@theme' => $theme, '!deleted_vars' => $deleted)), 'status');
}
